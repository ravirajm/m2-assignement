<?php
declare(strict_types=1);

namespace Raviraj\Checkout\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;

/**
 * Created by PhpStorm.
 * User: ravirajm
 * Date: 19/3/20
 * Time: 2:30 PM
 */
class ConfigProvider implements ConfigProviderInterface
{
    /** @var LayoutInterface */
    private $layout;

    /** @var string */
    private $blockId;

    /**
     * @param LayoutInterface $layout
     * @param string $blockId
     */
    public function __construct(LayoutInterface $layout, string $blockId)
    {
        $this->layout = $layout;
        $this->blockId = $blockId;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return [
            'cms_block' => $this->getPaymentCmsBlock()
        ];
    }

    /**
     * @return string
     */
    private function getPaymentCmsBlock(): string
    {
        return $this->layout->createBlock('Magento\Cms\Block\Block')
            ->setBlockId($this->blockId)
            ->toHtml();
    }
}
