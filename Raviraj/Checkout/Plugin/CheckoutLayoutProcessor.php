<?php
declare(strict_types=1);

namespace Raviraj\Checkout\Plugin;

use Magento\Checkout\Block\Checkout\LayoutProcessor;

/**
 * Created by PhpStorm.
 * User: ravirajm
 * Date: 19/3/20
 * Time: 2:30 PM
 */
class CheckoutLayoutProcessor
{
    /** @var string */
    private const TEMPLATE_NAME = 'Raviraj_Checkout/payment-cms-block';

    /**
     * @param LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        LayoutProcessor $subject,
        array $jsLayout
    ): array {
       $paymentList = &$jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']['payments-list'];

       // Check if the payment methods are present
        if (isset($paymentList['children'])) {
            $paymentList['children']['before-place-order']['children']['cms_block'] = [
                'component' => 'uiComponent',
                'config' => [
                    'template' => self::TEMPLATE_NAME,
                ],
            ];
        }

        return $jsLayout;
    }
}
