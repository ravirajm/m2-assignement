# Raviraj Checkout Module

This module adds the payment_cms_block on every payment method on checkout page.

# Installation and Setup

1. Download the Package
2. Keep it in app/code directory
3. Create a New cms block with id payment_cms_block at backend
4. Run php bin/magento setup:upgrade - To install the package on local system
5. Make sure if any of the payment method is enabled for the checkout.
6. Run the most used magento commands . like cache:flush , di:compile


#Additional Information

After the successful installation of the module a cms block payment_cms_block will be visible on payment method before placeorder.